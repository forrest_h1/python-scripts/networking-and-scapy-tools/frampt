#!/usr/bin/python3
#
#Frampt.py - Simple URL SSL Validation Tool
#
#Written by Forrest Hooker, 03/08/2023

## Modules ##

from time import sleep

import requests

import validators

import sys

import whois

#whois basically reads all whois information as a dict
#possible whois key values to print:
#domain_name
#registrar
#whois_server
#referral_url
#updated_date
#creation_date
#expiration_date
#name_servers
#status
#emails
#dnssec
#name
#org
#address
#city
#state
#zipcode
#country


## Vars/Arrs ##


header=("Frampt v1.1 - https://gitlab.com/forrest_h1/python-scripts/networking-and-scapy-tools/frampt")


## Classes ##


#Set color class for printing (Delete Vars as needed)
class color:
    RESET = '\033[0m'
    BOLD = '\033[1m'
    U_LINE = '\033[4m'
    GREEN = '\033[32m'
    RED = '\033[31m'
    YELLOW = '\033[33m'
    WHITE = '\033[37m'
    #Combo Vars (Testing new way of defining these
    B_U_LINE= ('{}{}'.format(BOLD,U_LINE))
    B_GREEN = ('{}{}'.format(BOLD,GREEN))
    B_RED = ('{}{}'.format(BOLD,RED))
    B_YELLOW = ('{}{}'.format(BOLD,YELLOW))
    B_WHITE = ('{}{}'.format(BOLD,WHITE))
    B_U_WHITE = ('{}{}'.format(B_U_LINE,WHITE))
    chk = '\u2713'
    skull = '\u2620'
    warn = '\u26A0'
    _x_ = '\u0078'


## Functions ##


def __main__():
    print("\n{}{}{}\n".format(color.B_U_WHITE, header, color.RESET))
    __argCheck__()
    __sslCheck__()
    __whoIsCheck__()

#Simple arg check, doesn't need getopt
def __argCheck__():
    global sslURL
    if len(sys.argv) == 1:
        __usage__()
    else:
        usrURL = sys.argv[1]
        if "http://" in usrURL:
            print("{}HTTP detected - {}{}.".format(color.YELLOW, color.warn, color.RESET))
            print("{}Replacing with {}https://...{}\n".format(color.YELLOW, color.BOLD, color.RESET))
            usrURL = usrURL.replace("http://", "https://")
        if not "https://" in usrURL or not "http://" in usrURL:
            usrURL = str(('https://{}'.format(usrURL)))
        if validators.url(usrURL):
            sslURL = usrURL
            print("{}Validated {} - {}{}\n".format(color.B_GREEN, usrURL,color.chk, color.RESET))
        else:
            print("{}Invalid URL given. Please enter a valid address.\n{}".format(color.RED, color.RESET))
            __usage__()

#Stupid simple function to do this, lol.
def __sslCheck__():
    #TRY to get a response using the given URL.
    try:
        #Define response as the result of a requests.get() on the given URL
        response = requests.get(sslURL)
        print("{}SSL Status - {}{}\n".format(color.B_GREEN, color.chk, color.RESET))
        print("{}{}{}{}{} appears to have a valid SSL Certificate.{}\n".format(color.GREEN, color.U_LINE, sslURL,color.RESET,color.GREEN, color.RESET))
    #If any exception, tell user site probably isn't safe.
    except:
        print("{}SSL Status - {}{}\n".format(color.B_RED, color._x_, color.RESET))
        print("{}{} does not have a valid SSL Certificate.{}\n".format(color.RED,sslURL,color.RESET))


def __whoIsCheck__():
    whoUrl = whois.whois(sslURL)
    print(whoUrl['registrar'])
    print(whoUrl['name'])
    if "PrivacyGuardian.org llc" in whoUrl['org']:
        offendOrg = whoUrl['org']
        print("{}KNOWN SPAM DOMAIN REGISTRAR DETECTED - {}{}".format(color.B_RED, offendOrg, color.RESET))

#Usage function
def __usage__():
    print("{}Frampt.py usage:{}".format(color.B_YELLOW, color.RESET))
    print("{}./frampt.py [https://YOUR_URL.COM] or [www.YOUR_URL.com]{}\n".format(color.YELLOW, color.RESET))
    sys.exit()





__main__()
