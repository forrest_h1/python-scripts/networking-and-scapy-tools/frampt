# Frampt
Frampt, originally just a simple SSL check utility, is a tool that attempts to check a given URL for anything that would indicate whether or not it's spam. This is the first tool I've written for both Windows + Linux compatibility in probably 8 years, so please feel free to raise issues.



The tool initially runs a very stupid and simple way of checking for valid HTTPS: trying to grab a webpage using the `requests` library. After this check, frampt will then look for any whois entries available from the site - what it's mainly looking for is registrar information and creation dates - a warning will be flagged if a Registrar is potentially linked to known spam blocks, but this does not necessarily indicate that a site itself is malicious.



The final check (With v1.1) is running the given domain as well as the registrar domain (if applicable) through `pydnsbl`, a utility for checking if a domain has been blacklisted by any spam filters.



More checks will be added as I iron out some of the kinks.

## Required Modules



- requests



- validators



- sys



- pydnsbl



- whois



## Usage



**LINUX** 



The only argument to the script is a valid url, which will be checked by validators. You do NOT need to add "https://" or "http://" as a prefix to the argument - it will automatically be added if it's not present.



`./frampt.py [www.YOUR_URL_HERE.com]` or `./frampt.py [http://www.YOUR_URL_HERE.com]`



**WINDOWS**



Coming soon!
